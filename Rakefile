##
## REQUIREMENTS:
##
## ruby
## sudo apt install optipng icnsutils inkscape
##

require 'fileutils'
require 'yaml'

##
## CONFIGURATION
##

output_directories = %[
android:
  res:
    drawable:
    drawable-land:
    drawable-ldpi:
    drawable-mdpi:
    drawable-sw600dp:
    drawable-sw600dp-port:
    drawable-xhdpi:
    drawable-xxhdpi:
    drawable-xxxhdpi:
    mipmap-hdpi:
    mipmap-mdpi:
    mipmap-xhdpi:
mac:
icons:
  white:
    16:
    22:
    32:
    64:
  black:
    16:
    22:
    32:
    64:
  state:
  logo:
web:
  22:
  32:
  64:
  128:
  qr:
linux:
  hicolor:
    24x24:
      apps:
    32x32:
      apps:
    48x48:
      apps:
    64x64:
      apps:
    128x128:
      apps:
    256x256:
      apps:
    scalable:
      apps:
branding:
  riseup:
    linux:
    mac:
    android:
      res:
        drawable:
        drawable-land:
        drawable-sw600dp:
        drawable-sw600dp-port:
        mipmap-hdpi:
        mipmap-mdpi:
        mipmap-xhdpi:
  calyx:
    linux:
    mac:
    android:
      res:
        drawable:
        drawable-land:
        drawable-sw600dp:
        drawable-sw600dp-port:
        mipmap-hdpi:
        mipmap-mdpi:
        mipmap-xhdpi:
]

android_launcher_target = [
  {:size => 48, :dpi => 160, :dest => 'android/res/mipmap-mdpi/ic_launcher.png'},
  {:size => 72, :dpi => 240, :dest => 'android/res/mipmap-hdpi/ic_launcher.png'},
  {:size => 96, :dpi => 320, :dest => 'android/res/mipmap-xhdpi/ic_launcher.png'},
  {:size => 96, :dpi => 320, :dest => 'android/res/drawable/ic_launcher.png'}
]

android_background_drawer_target = [
  {width: 1024, height: 512, dest: 'android/res/drawable/background_drawer.png'}
]

android_background_target = [
  {width: 793, height: 1586, dest: 'android/res/drawable/background_main.png'},
  {width: 1365, height: 668, dest: 'android/res/drawable-land/background_main.png'},
  {width: 1365, height: 928, dest: 'android/res/drawable-sw600dp/background_main.png'},
  {width: 793, height: 1586, dest: 'android/res/drawable-sw600dp-port/background_main.png'}
]

android_splash_target = [
  {width: 945, height: 1889, dest: 'android/res/drawable/ic_splash_background.png'},
  {width: 1365, height: 668, dest: 'android/res/drawable-land/ic_splash_background.png'},
  {width: 1365, height: 928, dest: 'android/res/drawable-sw600dp/ic_splash_background.png'},
  {width: 945, height: 1889, dest: 'android/res/drawable-sw600dp-port/ic_splash_background.png'}
]

android_logo_target = [
  {size: 171, dest: 'android/res/drawable/logo_square.png'}
]

linux_launcher_target = [
  {:size => 256, :dest => 'linux/launcher.png'}
]

mac_launcher_target = [
  {:size => 1024, :dest => 'mac/launcher.png'}
]

android_icon_target = [
  {:size => 18, :dpi => 120, :dest => 'android/res/drawable-ldpi'},
  {:size => 24, :dpi => 160, :dest => 'android/res/drawable-mdpi'},
  {:size => 36, :dpi => 240, :dest => 'android/res/drawable-hdpi'},
  {:size => 48, :dpi => 320, :dest => 'android/res/drawable-xhdpi'}
]

white_icon_target = [
  {:size => 16, :dest => 'icons/white/16'},
  {:size => 22, :dest => 'icons/white/22'},
  {:size => 32, :dest => 'icons/white/32'},
  {:size => 64, :dest => 'icons/white/64'}
]

black_icon_target = [
  {:size => 16, :dest => 'icons/black/16'},
  {:size => 22, :dest => 'icons/black/22'},
  {:size => 32, :dest => 'icons/black/32'},
  {:size => 64, :dest => 'icons/black/64'}
]

linux_target = [
  {:size => 24, :dest => 'linux/hicolor/24x24/apps/bitmask.png'},
  {:size => 32, :dest => 'linux/hicolor/32x32/apps/bitmask.png'},
  {:size => 48, :dest => 'linux/hicolor/48x48/apps/bitmask.png'},
  {:size => 64, :dest => 'linux/hicolor/64x64/apps/bitmask.png'},
  {:size => 128, :dest => 'linux/hicolor/128x128/apps/bitmask.png'},
  {:size => 256, :dest => 'linux/hicolor/256x256/apps/bitmask.png'}
]

svg_to_svg = [
  ['source/icons/state/*.svg', {:dest => 'icons/state'}]
]

#
# default target filetype is png, unless otherwise specified
#
svg_to_raster = [
  # icons
  ['source/icons/state/*.svg',         {:size => 128, :dest => 'icons/state', :bg => '0xffffff'}],
  ['source/icons/white/*.svg',         white_icon_target],
  ['source/icons/black/*.svg',         black_icon_target],
  ['source/leap/kid-square.svg',       {:size => 256,  :dest => 'icons/logo/leap256x256.png'}],
  ['source/leap/kid-jumping.svg',      {:width => 200, :dest => 'icons/logo/leap200.png'}],
  ['source/leap/kid-jumping.svg',      {:width => 128, :dest => 'icons/logo/leap128.png'}],
  ['source/leap/kid-jumping.svg',      {:width => 64,  :dest => 'icons/logo/leap64.png'}],

  # android
  ['source/android/icons/*.svg',       android_icon_target],
  #['source/android/leap-launcher.svg', android_launcher_target],
  #['source/android/leap-launcher.svg', {:size => 512, :dest => 'android/leap-icon.png'}],
  #['source/leap/kid-jumping-silhouette-light.svg', android_icon_target],
  #['source/android/vpn_disconnected.svg', android_icon_target],
  #['source/android/vpn_progress.svg',  android_icon_target],
  #['source/android/leap-debug-launcher.svg', android_launcher_target],
  #['source/android/leap-debug-launcher.svg', {:size => 512, :dest => 'android/leap-debug-icon.png'}],
  ['source/masks/mask-launcher.svg',      android_launcher_target],
  ['source/masks/mask-launcher.svg',      {:size => 512, :dest => 'android/hi-res-icon.png'}],
  ['source/masks/feature-graphic.svg',    {:width => 1024, :height => 512, :dest => 'android/feature-graphic.png'}],
  #['source/android/mask-silhouette.svg', android_icon_target],

  # mac
  ['source/masks/mask-launcher.svg', {:size => 1024, :dest => 'mac/bitmask-1024x1024.png'}],
  #['source/masks/mask-launcher-flat.svg', {:width => 32, :height => 26, :dest => 'mac/bitmask.tiff'}],
  #['source/statusbar/mac-menu-icon.svg', {:width => 22, :height => 21, :dest => 'mac/menubar-icon-22x21.png'}],
  #['source/statusbar/mac-menu-icon.svg', {:width => 44, :height => 42, :dest => 'mac/menubar-icon-44x42.png'}],

  # web
  ['source/leap/kid-jumping-bw.svg',   {:size => 16,  :dest => 'web/favicon-bw.ico'}],
  ['source/leap/kid-ico.svg',       {:size => 16,  :dest => 'web/favicon.ico'}],
  ['source/masks/mask.svg',            {:width => 128, :dest => 'web/128'}],
  ['source/web/masthead/*.svg',        {:dest => 'web/masthead'}],
  ['source/web/icons/*',               {:size => 32,  :dest => 'web/32'}],
  ['source/web/icons/*',               {:size => 64,  :dest => 'web/64'}],
  ['source/android/black/*.svg',       {:size => 22, :dest => 'web/22'}],

  # linux
  ['source/masks/mask-launcher.svg',   linux_target],

  # print
  ['source/leap/kid-jumping.svg',      {:width => 1000, :dest => 'print/leap.png'}],
  ['source/letterhead/letterhead.svg', {:width => 2400, :height => 300, :dest => 'print'}]
]

png_to_icns = [
  ['mac/bitmask-1024x1024.png', {:dest => 'mac/bitmask.icns'}]
]

copy = [
  ['source/qr-codes/*.png', {:dest => 'web/qr'}],
  ['source/masks/mask-launcher.svg', {:dest => 'linux/hicolor/scalable/apps/bitmask.svg'}]
]

png_to_gif = [
  ['icons/state/wait-*.png', {:dest => 'icons/state/wait.gif', :delay => 50}]
]

##
## CUSTOM BRANDS
##

def brand_path(prefix, paths)
  paths.map {|i| i = i.clone; i[:dest] = "branding/" + prefix + "/" + i[:dest]; i}
end

['riseup', 'calyx'].each do |brand|
  svg_to_raster << ["source/#{brand}/launcher.svg",   brand_path(brand, android_launcher_target)]
  svg_to_raster << ["source/#{brand}/logo.svg",       brand_path(brand, android_logo_target)]
  svg_to_raster << ["source/#{brand}/feature.svg",    brand_path(brand, android_background_drawer_target)]
  svg_to_raster << ["source/#{brand}/background.svg", brand_path(brand, android_background_target)]
  svg_to_raster << ["source/#{brand}/feature.svg",    brand_path(brand, android_splash_target)]
  svg_to_raster << ["source/#{brand}/launcher.svg",   brand_path(brand, linux_launcher_target)]
  svg_to_raster << ["source/#{brand}/launcher.svg",   brand_path(brand, mac_launcher_target)]
  copy << ["source/#{brand}/launcher.svg", {:dest => "branding/#{brand}/linux/launcher.svg"}]
  png_to_icns << ["branding/#{brand}/mac/launcher.png", {:dest => "branding/#{brand}/mac/launcher.icns"}]
end

##
## HELPERS
##

def run(cmd)
  system(cmd + ' >/dev/null 2>/dev/null')
  unless $? == 0
    puts "ERROR: failed to run #{cmd}"
    puts "bailing out."
    exit
  end
end

#
# inkscape command line tool only lets you grab dimensions of the image,
# not the whole page. So, we just grep for the first height/width.
#
def dimensions(file)
  width, height = File.read(file).match(/width="(\d+)"[\s\n]+height="(\d+)"/m)[1..2]
  return [width.to_i, height.to_i]
rescue Exception
  puts "WARNING: could not get dimensions from file #{file}"
  return [0, 0]
end

def render_svg_to_raster(source, targets)
  render_changed(source, targets) do |src_file, dest_file, target|
    filetype = File.extname(dest_file)
    width = height = nil
    if target[:size]
      height = width = target[:size]
    else
      height = target[:height]
      width = target[:width]
    end
    if filetype != '.png'
      real_dest_file = dest_file
      dest_file = dest_file.sub(/#{filetype}$/, '-tmp.png')
    end
    options = ["--file=#{src_file}", "--export-png=#{dest_file}"]
    if target[:clear]
      options << "--export-background=0xffffff" << "--export-background-opacity=0x00"
    elsif target[:bg]
      options << "--export-background=#{target[:bg]}"
    end
    if width || height
      if width && height
        options << "-w #{width}" << "-h #{height}"
        src_width, src_height = dimensions(src_file)
        src_ratio = src_width.to_f / src_height
        ratio     = width.to_f / height
        if ratio != src_ratio
          if target[:stretch].nil? || target[:stretch] == :height
            h = src_width / ratio
            options << "--export-area=%i:%i:%i:%i" % [0,0,src_width,h]
          elsif target[:stretch] == :width
            w = src_height * ratio
            options << "--export-area=%i:%i:%i:%i" % [0,0,w,src_height]
          end
        end
      elsif width
        options << "-w #{width}"
      elsif height
        options << "-h #{height}"
      end
    elsif target[:dpi]
      options << "--export-dpi=#{target[:dpi]}"
    end
    run("inkscape #{options.join ' '}")
    run("optipng #{dest_file}")
    if filetype != '.png'
      if filetype == '.ico'
        # only imagemagick supports writing to .ico
        run("convert #{dest_file} #{real_dest_file}")
      else
        run("gm convert #{dest_file} #{real_dest_file}")
      end
      File.unlink(dest_file)
    end
  end
end

# clean up a SVG to a smaller SVG
def render_svg_to_svg(source, targets)
  render_changed(source, targets) do |src_file, dest_file|
    options = [
      "--export-filename='#{dest_file}'",
      "--export-overwrite",
      "--export-type=svg",
      "--export-plain-svg"
    ]
    run("inkscape '#{src_file}' #{options.join ' '}")
  end
end

def render_png_to_icns(source, targets)
  render_changed(source, targets) do |src_file, dest_file|
    run("png2icns #{dest_file} #{src_file}")
  end
end

def copy_files(source, targets)
  render_changed(source, targets) do |src_file, dest_file|
    run("cp '#{src_file}' '#{dest_file}'")
  end
end

def render_png_to_gif(source, targets)
  render_changed(source, targets, noglob: true) do |src_file, dest_file, target|
    options = []
    if target[:delay]
      options << "-delay #{target[:delay]}"
    end
    if target[:loop]
      options << "-loop #{target[:loop]}"
    end
    src_file = src_file.gsub('STAR', '*') # unescape the globbing
    run("convert #{options.join(' ')} #{src_file} #{dest_file}")
  end
end

#
# for a source and target(s), yields (src_file, dest_file, info) for each
# source and destination pair that needs rendering.
#
def render_changed(source, targets, noglob: false, &block)
  if source !~ /\*/ && !File.exist?(source)
    puts "Missing file: #{source}"
    return
  end
  Dir.glob(source).each do |src_file|
    if !File.exist?(src_file)
      puts "Missing file: #{src_file}"
      next
    end
    [targets].flatten.each do |target|
      if File.directory?(target[:dest])
        dest_file = File.join(target[:dest], File.basename(src_file).sub(/\.svg$/,'.png'))
      else
        dest_file = target[:dest]
      end
      if !File.exists?(dest_file) || File.mtime(dest_file) < File.mtime(src_file)
        if noglob
          yield source, dest_file, target
          progress
          return
        else
          yield src_file, dest_file, target
          progress
        end
      end
    end
  end
end

def progress
  putc '.'
  STDOUT.flush
end

def create_directories(dir_hash)
  dir_hash.each do |k,v|
    FileUtils.mkdir_p(k.to_s)
    if v
      Dir.chdir(k.to_s) do
        create_directories(v)
      end
    end
  end
end

##
## RENDER TASK
##

task :default => :render

desc "render SVG images"
task :render do
  Dir.chdir(File.dirname(__FILE__)) do
    create_directories(YAML.parse(output_directories).to_ruby)
    svg_to_svg.each do |source, targets|
      render_svg_to_svg(source, targets)
    end
    svg_to_raster.each do |source, targets|
      render_svg_to_raster(source, targets)
    end
    png_to_icns.each do |sources, targets|
      render_png_to_icns(sources, targets)
    end
    copy.each do |sources, targets|
      copy_files(sources, targets)
    end
    png_to_gif.each do |sources, targets|
      render_png_to_gif(sources, targets)
    end
  end
  puts
  puts "Done."
end

desc "clean out rendered images"
task :clean do
  Dir.chdir(File.dirname(__FILE__)) do
    output_directories.each do |dir|
      Dir.entries(dir).grep(/\.(png|icns|jpg)$/).each do |file|
        File.unlink File.join(dir,file)
      end
    end
  end
end

